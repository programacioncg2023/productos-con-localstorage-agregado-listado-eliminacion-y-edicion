//ASIGNACIÓN DE VARIABLES A ELEMENTOS DEL DOM
let titulo = document.querySelector("#titulo");
let seccion_producto = document.querySelector("#producto");
let descripcion = document.querySelector('#desc');
let url_imagen = document.querySelector('#dirImg');
let boton_uno = document.querySelector('#btn_uno');
let boton_dos = document.querySelector('#btn_dos');

//DECLARO EL ARRAY DE PRODUCTOS, VACÍO.
let arrayProductos = [];
//DECLARO EL FORMATO DEL PRODUCTO, NO NECESARIO, PERO INFORMATIVO
let producto = {
    descripcion: "",
    imagen: ""
}
//VARIABLES AUXILIARES
let des, img;

if (localStorage.getItem('productos')) {
    arrayProductos = JSON.parse(localStorage.productos);
    console.log(arrayProductos);
} else {
    //ASIGNO MANUALMENTE EL PRIMER PRODUCTO
    //fuente de productos: https://revistabyte.es/recomendamos/productos-tic-mas-vendidos-amazon/

    producto = {
            descripcion: "Fire TV Stick 4K Ultra HD con mando por voz Alexa",
            imagen: "https://revistabyte.es/wp-content/uploads/2020/05/1-323x360.jpg.webp"
        }
        //LO AGREGO AL ARRAY
    arrayProductos.push(producto);

    //ASIGNO MANUALMENTE EL SEGUNDO PRODUCTO
    producto = {
        descripcion: 'TV Samsung 4K UHD de 50”',
        imagen: "https://revistabyte.es/wp-content/uploads/2020/05/4-425x360.jpg.webp"
    }
    arrayProductos.push(producto);
}
//INYECTO LOS PRODUCTOS DEL ARRAY EN LA SECCIÓN PRODUCTO
seccion_producto.innerHTML = armaTemplate();


//FUNCIÓN QUE SE INVOCA DESDE LA INTERFAZ PARA CARGAR NUEVOS ARTÍCULOS
function agregar() {
    //TOMO LOS VALUES DE DESCRIPCIÓN E IMAGEN DE LA INTERFAZ
    des = descripcion.value.trim();
    img = url_imagen.value.trim();
    //PARA EVALUAR QUE HAYA INGRESADO ALGO
    if (des.length === 0 || img.length === 0) return;

    //ARMO EL OBJETO
    producto = {
            descripcion: des,
            imagen: img
        }
        //AGREGO EL PRODUCTO AL ARRAY
    arrayProductos.push(producto);
    console.log(arrayProductos);

    //SIGO EL MISMO PROCEDIMIENTO QUE CUANDO SE HACÍA MANUAL, LÍNEA 112
    seccion_producto.innerHTML = armaTemplate();
}

//FUNCION QUE ARMA EL TEMPLATE DE ARTÍCULOS CON LOS PRODUCTOS DEL ARRAY Y RETORNA EL TEMPLATE
function armaTemplate() {
    let template = '';
    for (let i = 0; i < arrayProductos.length; i++) {
        producto = arrayProductos[i];
        template += `<article>
                        <div class="trash" onclick="eliminarItem(${i})"><img src="eliminar.png"></div>
                        <div class="edit" onclick="editarItem(${i})"><img src="edit1.png"></div>
                        <h3 class="descripcion">${producto.descripcion}</h3>
                        <img src="${producto.imagen}" class="imagen">
                    </article>`
    }
    return template;
}

function listado() {
    if (arrayProductos.length > 0) localStorage.setItem("productos", JSON.stringify(arrayProductos));
    location.href = "resultados.html";
}

function eliminarItem(nroProd) {
    console.log("Hice click en el producto: ", nroProd);
    arrayProductos.splice(nroProd, 1);
    if (arrayProductos.length === 0) {
        localStorage.removeItem('productos');
    }else{
        localStorage.setItem("productos", JSON.stringify(arrayProductos));
    }
    seccion_producto.innerHTML = armaTemplate();
}

function editarItem(nroProd) {
    document.querySelector("#producto").innerHTML = armaTemplateSinBtns();
    document.querySelector("#titulo").innerHTML = "Edición de Producto";
    console.log("Hice click en el producto: ", nroProd);

    producto = arrayProductos[nroProd];
    console.log(producto.descripcion);
    descripcion.value = producto.descripcion;
    url_imagen.value = producto.imagen;

    boton_uno.value = "Modificar";
    boton_uno.classList.add("color_green");
    boton_dos.value = "Cancelar";
    boton_dos.classList.add("color_red");
    boton_uno.setAttribute("onclick", `modificar(${nroProd})`);
    boton_dos.setAttribute("onclick", "limpiar_cancelar()");
}

function limpiar_cancelar() {
    descripcion.value = "";
    url_imagen.value = "";
    titulo.innerHTML = "Nuevo Producto";
    boton_uno.value = "Agregar";
    boton_uno.classList.remove("color_green");
    boton_dos.value = "Listado";
    boton_dos.classList.remove("color_red");
    boton_uno.setAttribute("onclick", "agregar()");
    boton_dos.setAttribute("onclick", "listado()");

    seccion_producto.innerHTML = armaTemplate();
    localStorage.setItem("productos", JSON.stringify(arrayProductos));
}

function modificar(nroProd) {
    console.log("Entré en modificar: ", nroProd);
    des = descripcion.value.trim();
    img = url_imagen.value.trim();
    if (des.length === 0 || img.length === 0) return;

    producto = {
        descripcion: des,
        imagen: img
    }

    arrayProductos[nroProd] = producto;
    limpiar_cancelar();
}

function armaTemplateSinBtns() {
    let template = '';
    for (let i = 0; i < arrayProductos.length; i++) {
        producto = arrayProductos[i];
        template += `<article>
                        <h3 class="descripcion">${producto.descripcion}</h3>
                        <img src="${producto.imagen}" class="imagen">
                    </article>`
    }
    return template;
}